package com.mingbikes.beaconmock.utils;

import java.util.UUID;

/**
 * Created by charles on 17/7/3.
 */

public class BluetoothUUID {

    public static final UUID bleServerUUID = UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825");
    public static final UUID nameServerUUID = UUID.fromString("fda53366-a4e2-4fb1-afcf-c6eb07647825");
    public static final UUID pawdServerUUID = UUID.fromString("fda51437-a4e2-4fb1-afcf-c6eb07647825");
}

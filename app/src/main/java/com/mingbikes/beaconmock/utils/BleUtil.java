package com.mingbikes.beaconmock.utils;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.AdvertiseData;
import android.os.ParcelUuid;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

/**
 * ble util
 */
public class BleUtil {

    //设置scan广播数据
    public static AdvertiseData createScanAdvertiseData(short major, short minor, byte txPower) {
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
        builder.setIncludeDeviceName(true);

        byte[] serverData = new byte[5];
        ByteBuffer bb = ByteBuffer.wrap(serverData);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.putShort(major);
        bb.putShort(minor);
        bb.put(txPower);
        builder.addServiceData(ParcelUuid.fromString(BluetoothUUID.bleServerUUID.toString())
                , serverData);

        AdvertiseData adv = builder.build();
        return adv;
    }

    /**
     * create AdvertiseDate for iBeacon
     */
    public static AdvertiseData createIBeaconAdvertiseData(UUID proximityUuid, short major, short minor, byte txPower) {

        String[] uuidstr = proximityUuid.toString().replaceAll("-", "").toLowerCase().split("");
        byte[] uuidBytes = new byte[16];
        for (int i = 1, x = 0; i < uuidstr.length; x++) {
            uuidBytes[x] = (byte) ((Integer.parseInt(uuidstr[i++], 16) << 4) | Integer.parseInt(uuidstr[i++], 16));
        }
        byte[] majorBytes = {(byte) (major >> 8), (byte) (major & 0xff)};
        byte[] minorBytes = {(byte) (minor >> 8), (byte) (minor & 0xff)};
        byte[] mPowerBytes = {txPower};
        byte[] manufacturerData = new byte[0x17];
        byte[] flagibeacon = {0x02, 0x15};

        System.arraycopy(flagibeacon, 0x0, manufacturerData, 0x0, 0x2);
        System.arraycopy(uuidBytes, 0x0, manufacturerData, 0x2, 0x10);
        System.arraycopy(majorBytes, 0x0, manufacturerData, 0x12, 0x2);
        System.arraycopy(minorBytes, 0x0, manufacturerData, 0x14, 0x2);
        System.arraycopy(mPowerBytes, 0x0, manufacturerData, 0x16, 0x1);
//
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
        builder.addManufacturerData(0x004c, manufacturerData);

        AdvertiseData adv = builder.build();
        return adv;
    }

    public static AdvertiseData creatManufacturerData(byte[] manufacturerData){
        AdvertiseData.Builder builder = new AdvertiseData.Builder();
//        builder.addServiceData(new ParcelUuid(BluetoothUUID.nameServerUUID) ,manufacturerData);
//        builder.addServiceData(new ParcelUuid(BluetoothUUID.pawdServerUUID) ,manufacturerData);
        BluetoothGattCharacteristic nameCharacter = new BluetoothGattCharacteristic(
                BluetoothUUID.nameServerUUID,
                BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ);
        BluetoothGattCharacteristic pswdCharacter = new BluetoothGattCharacteristic(
                BluetoothUUID.pawdServerUUID,
                BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ);
        BluetoothGattService service = new BluetoothGattService(BluetoothUUID.bleServerUUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);
        service.addCharacteristic(pswdCharacter);
        service.addCharacteristic(nameCharacter);
        builder.addServiceUuid(new ParcelUuid(BluetoothUUID.bleServerUUID));
//        builder.addManufacturerData(0x0011,manufacturerData);
//        builder.addManufacturerData(0x0013,manufacturerData);
        return builder.build();
    }
}
